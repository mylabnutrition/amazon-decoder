# amazon-decoder

The Amazon Decoder Library is a small library developed by MyLabNutrition.net for handling the amazon TSV export 
of sold items, with the purpouse of making Recepits and Invoices. 

This Library is free to use, to share and to edit, since we believe that Open Source will make the world a better place.
 
This library is under the MIT Software License. 
 

[#StayOpenSource](https://twitter.com/hashtag/StayOpenSource?src=hash) [#StayOpenMindend](https://twitter.com/hashtag/StayOpenMinded?src=hash)
 
[Arcangelo Vicedomini, CTO @ MyLab Nutrition](https://www.linkedin.com/in/arcangelovicedomini/)

# Usage

Add it with Maven, simply and fast, using the jitpack repository

```
<repositories>
  <repository>
    <id>jitpack.io</id>
    <url>https://jitpack.io</url>
  </repository>
</repositories>
```

```
<dependency>
  <groupId>org.bitbucket.mylabnutrition</groupId>
  <artifactId>amazon-decoder</artifactId>
  <version>1.0.0.RELEASE</version>
</dependency>
```