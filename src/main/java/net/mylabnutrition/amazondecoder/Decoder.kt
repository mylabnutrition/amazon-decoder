package net.mylabnutrition.amazondecoder

import net.mylabnutrition.amazondecoder.exceptions.AmazonDecoderException
import net.mylabnutrition.amazondecoder.exceptions.DecoderError
import net.mylabnutrition.amazondecoder.utils.OrderLineReader
import net.mylabnutrition.amazondecoder.utils.getISODateFromString
import net.mylabnutrition.amazondecoder.valueobjects.*
import org.apache.commons.lang3.StringUtils
import java.lang.Double
import java.time.ZoneId
import java.util.*

/**
 * Decodes your amazon txt file into an object hierarcy structure.
 */
class Decoder(private val headerLine: String, private val zoneIdName: String = "CET") {
    private var hasHeader = false
    private var hasLines = false
    private val positionMapper = mutableMapOf<String, Int>()
    private val orders = mutableMapOf<String, Order>()
    private val zoneId = ZoneId.of(zoneIdName)

    init {
        if (StringUtils.isEmpty(headerLine)) throw AmazonDecoderException(DecoderError.EMPTY_HEADER)
        val headers = headerLine.split('\t')
        for (i in 0..headers.size) {
            positionMapper[headers[i]] = i
        }
        hasHeader = true
    }

    /**
     * Returns all the orders in an OOP way, you must first add lines from your amazon txt file.
     * if no line is added, it will go in error
     *
     */
    fun getOrders(): List<Order> {
        if (!hasLines) throw AmazonDecoderException(DecoderError.NO_LINES)
        val l: MutableList<Order> = mutableListOf()
        for ((k, v) in orders) {
            l.add(v.copy())
        }
        return l.toList()
    }


    /**
     * Returns all the orders in an OOP way, inside a <String, Order> map, the string argument is the ID of the Amazon Order, you must first add lines from your amazon txt file.
     * if no line is added, it will go in error
     *
     */
    fun getOrdersMap(): Map<String, Order> {
        if (!hasLines) throw AmazonDecoderException(DecoderError.NO_LINES)
        val m = mutableMapOf<String, Order>()
        for ((k, v) in orders) {
            m[k] = v.copy()
        }
        return m.toMap()
    }

    /**
     * Evaluates a line from your amazon txt file
     */
    fun addLine(line: String) {
        if (StringUtils.isEmpty(line)) throw AmazonDecoderException(DecoderError.EMPTY_LINE)
        hasLines = true

        val orderLine = line.split('\t')

        val olr = OrderLineReader(orderLine, positionMapper)

        val id = orderLine[positionMapper["amazon-order-id"]!!]
        val order = orders[id] ?: Order(
                olr.getLineElementF("amazon-order-id"),
                olr.getLineElement("merchant-order-id"),
                getISODateFromString(olr.getLineElementF("purchase-date"), zoneId),
                getISODateFromString(olr.getLineElementF("last-updated-date"), zoneId),
                OrderStatus.fromAMZStr(olr.getLineElementF("order-status")),
                FullfillmentChannel.fromAMZStr(olr.getLineElementF("fulfillment-channel")),
                olr.getLineElement("sales-channel"),
                olr.getLineElement("order-channel"),
                olr.getLineElement("url"),
                olr.getLineElement("ship-service-level"),
                Currency.getInstance(olr.getLineElementF("currency")),
                Double.parseDouble(olr.getLineElement("shipping-price") ?: "0.0"),
                Double.parseDouble(olr.getLineElement("shipping-tax") ?: "0.0"),
                Double.parseDouble(olr.getLineElement("gift-wrap-price") ?: "0.0"),
                Double.parseDouble(olr.getLineElement("gift-wrap-tax") ?: "0.0"),
                olr.getLineElement("item-promotion-discount"),
                olr.getLineElement("ship-promotion-discount"),
                Shipment(
                        olr.getLineElement("ship-city"),
                        olr.getLineElement("ship-state"),
                        olr.getLineElement("ship-postal-code"),
                        olr.getLineElement("ship-country")
                ),
                olr.getLineElement("promotion-ids"),
                olr.getLineElement("fullfilled-by")
        )

        order.products.add(Product(
                olr.getLineElementF("product-name"),
                olr.getLineElementF("sku"),
                olr.getLineElementF("asin"),
                ItemStatus.fromAMZStr(olr.getLineElementF("item-status")),
                Integer.parseInt(olr.getLineElementF("quantity")),
                Double.parseDouble(olr.getLineElement("item-price") ?: "0.0"),
                Double.parseDouble(olr.getLineElement("item-tax") ?: "0.0")
        ))

        if (orders[id] == null) orders[id] = order
    }
}