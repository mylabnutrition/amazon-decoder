package net.mylabnutrition.amazondecoder.valueobjects

data class Shipment(
        val city: String?,
        val state: String?,
        val postalCode: String?,
        val country: String?
)