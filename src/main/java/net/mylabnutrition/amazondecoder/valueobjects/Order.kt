package net.mylabnutrition.amazondecoder.valueobjects

import java.time.ZonedDateTime
import java.util.*

data class Order(
        val id: String,
        val merchOrdId: String?,
        val purchaseDate: ZonedDateTime,
        val lastUpdatedDate: ZonedDateTime,
        val orderStatus: OrderStatus,
        val fulfillmentChannel: FullfillmentChannel,
        val salesChannel: String?,
        val orderChannel: String?,
        val url: String?,
        val shipServiceLevel: String?,
        val currency: Currency,
        val shippingPrice: Double,
        val shippingTax: Double,
        val giftWrapPrice: Double,
        val giftWrapTax: Double,
        val itemPromotionDiscount: String?,
        val shipmentPromotionDiscount: String?,
        val shipment: Shipment?,
        val promotionIds: String?,
        val fullfilledBy: String?,
        val products: MutableList<Product> = mutableListOf()
)