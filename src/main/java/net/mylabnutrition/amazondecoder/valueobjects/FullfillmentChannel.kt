package net.mylabnutrition.amazondecoder.valueobjects

enum class FullfillmentChannel {
    MERCHANT, AMAZON;

    companion object {
        fun fromAMZStr(str: String) = FullfillmentChannel.valueOf(str.toUpperCase())
    }
}