package net.mylabnutrition.amazondecoder.valueobjects

enum class ItemStatus {
    SHIPPED, UNSHIPPED;

    companion object {
        fun fromAMZStr(str: String) = ItemStatus.valueOf(str.toUpperCase())
    }

}