package net.mylabnutrition.amazondecoder.valueobjects

data class Product(
        val name: String,
        val sku: String,
        val asin: String,
        val status: ItemStatus,
        val quantity: Int,
        val price: Double,
        val tax: Double
)