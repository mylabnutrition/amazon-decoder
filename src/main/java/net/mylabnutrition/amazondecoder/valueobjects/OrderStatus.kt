package net.mylabnutrition.amazondecoder.valueobjects

enum class OrderStatus {
    SHIPPED, PENDING, CANCELED;

    companion object {
        fun fromAMZStr(str: String) = OrderStatus.valueOf(str.toUpperCase())
    }
}