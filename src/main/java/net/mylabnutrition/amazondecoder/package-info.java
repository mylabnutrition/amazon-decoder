/**
 * The Amazon Decoder Library is a small library developed by
 * MyLabNutrition.net for handling the amazon TSV export of
 * sold items, with the purpouse of making Recepits and Invoices.
 * <p>
 * This Library is free to use, to share and to edit, since we believe
 * that Open Source will make the world a better place.
 * <p>
 * This library is under the MIT Software License.
 * <p>
 * #StayOpenSource #StayOpenMindend
 * <p>
 * <a href="https://www.linkedin.com/in/arcangelovicedomini/">Arcangelo Vicedomini, CTO @ MyLab Nutrition</a>
 */
package net.mylabnutrition.amazondecoder;