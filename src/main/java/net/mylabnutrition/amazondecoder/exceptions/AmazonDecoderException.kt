package net.mylabnutrition.amazondecoder.exceptions

class AmazonDecoderException(error: DecoderError, message: String = "") : RuntimeException("${error.name} $message")
