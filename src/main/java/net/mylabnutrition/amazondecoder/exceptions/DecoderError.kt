package net.mylabnutrition.amazondecoder.exceptions

enum class DecoderError {
    EMPTY_HEADER, EMPTY_LINE, NO_LINES, HEADER_NOT_FOUND, VALUE_FOR_HEADER_NOT_FOUND
}
