package net.mylabnutrition.amazondecoder.utils

import java.time.LocalDateTime
import java.time.ZoneId
import java.time.format.DateTimeFormatter

fun getISODateFromString(date: String, zoneId: ZoneId) = LocalDateTime.parse(date, DateTimeFormatter.ISO_DATE_TIME).atZone(zoneId)!!