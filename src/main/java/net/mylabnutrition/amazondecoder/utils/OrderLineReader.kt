package net.mylabnutrition.amazondecoder.utils

import net.mylabnutrition.amazondecoder.exceptions.AmazonDecoderException
import net.mylabnutrition.amazondecoder.exceptions.DecoderError

class OrderLineReader(private val orderLine: List<String>, private val positionMapper: Map<String, Int>) {
    fun getLineElement(name: String): String? = orderLine[positionMapper[name]
            ?: throw AmazonDecoderException(DecoderError.HEADER_NOT_FOUND, name)]

    fun getLineElementF(name: String): String = getLineElement(name)
            ?: throw AmazonDecoderException(DecoderError.VALUE_FOR_HEADER_NOT_FOUND, "header $name, order line ${getLineElement("amazon-order-id")}")
}